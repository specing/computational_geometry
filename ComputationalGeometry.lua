-- Computational geometry main implementation file
-- Copyright (C) 2019 Fedja Beader
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.



-- TODO: right turn DONE
-- TODO: triangulation with output for rendering DONE
-- TODO: (fast) rendering
-- TODO: point location in the given monotone polygon for given points DONE
-- TODO: check if polygon is x monotone
-- TODO: drop assumption of general position
-- TODO: The above for non-monotone polygons.

-- M is an x-monotone polygon

M = {
  {1,4},
  {2,2},
  {3,3},
  {4,1},
  {7,3},
  {8,2},
  {10,3},
  {9,6},
  {8,5},
  {6,7},
  {5,5},
  {3,6}
}

--[[ Sorted by x.
  {1,4},
  {2,2},
  {3,3},
  {3,6}
  {4,1},
  {5,5},
  {6,7},
  {7,3},
  {8,2},
  {8,5},
  {9,6},
  {10,3},
--]]

--[[
-- Left-top half circle (same path test)
M = {
  {0,0},
  {1,3},
  {2,5},
  {3,6},
  {4,6}
}
--]]

----[[
-- extended trapezoid (different path test)
M = {
  {0,0},
  {1,1},
  {3,1},
  {5,1},
  {4,0},
  {2,0}
}
----]]

local sin = math.sin
local cos = math.cos
local pi  = math.pi
local r = 2

-- Warzone2100-like hexagonal button
M_Hexagon = {
  {r*cos(pi*7/6), r*sin(pi*7/6)},
  {r*cos(pi*5/6), r*sin(pi*5/6)},
  {0, r},
  {r*cos(pi*1/6), r*sin(pi*1/6)},
  {r*cos(pi*11/6), r*sin(pi*11/6)},
  {0, -r}
}

-- M = M_Hexagon
-- When D(p1, p2, p3) < 0, then (p2,p3) is a right turn relative to vector (p1,p2);
-- When D(p1, p2, p3) > 0, then (p2,p3) is a left turn relative to vector (p1,p2);
-- When D(p1, p2, p3) = 0, then p1, p2, p3 are colinear.
local function Determinant(p1, p2, p3)
	return p2[1]*p3[2] + p1[1]*p2[2] + p1[2]*p3[1]
	     - p2[1]*p1[2] - p3[1]*p2[2] - p3[2]*p1[1]
end

local function IsRightTurn(p1, p2, p3)
	return Determinant(p1, p2, p3) < 0
end

local function IsLeftTurn(p1, p2, p3)
	return Determinant(p1, p2, p3) > 0
end

local function SingleStepModulo(x, N)
	if x > 0 then
		if x <= N then
			return x
		elseif x == N+1 then
			return 1
		end
	elseif x == 0 then
		return N
	end
	error("Parameter out of range: " .. x)
end

local function ListToString(L)
	return table.concat(L, " ")
end

-- Input is a polygon M with at least 3 vertices given in order as they are connected
--       and the index in M of the vertex with the lowest x coordinate
-- Output is a table of indexes to M
-- O(n) time, O(n) space
local function SortMonotone(M, lowest_x)
	local sorted_vertices = {lowest_x}

	local walk_i_left =  SingleStepModulo(lowest_x-1, #M)
	local walk_i_right = SingleStepModulo(lowest_x+1, #M)
	local xl, xr
	-- Guaranteed different indices, as we have at least 4 vertices
	while true do
		xl = M[walk_i_left][1]
		xr = M[walk_i_right][1]

		if xl < xr then
			sorted_vertices[#sorted_vertices + 1] = walk_i_left
			walk_i_left = SingleStepModulo(walk_i_left - 1, #M)
		elseif xr < xl then
			sorted_vertices[#sorted_vertices + 1] = walk_i_right
			walk_i_right = SingleStepModulo(walk_i_right + 1, #M)
		else -- Same x coordinate, take left walk (lower)
			if M[walk_i_left][2] < M[walk_i_right][2] then
				sorted_vertices[#sorted_vertices + 1] = walk_i_left
				walk_i_left = SingleStepModulo(walk_i_left - 1, #M)
			else
				sorted_vertices[#sorted_vertices + 1] = walk_i_right
				walk_i_right = SingleStepModulo(walk_i_right + 1, #M)
			end
		end

		vi = sorted_vertices[#sorted_vertices]
		print ("SortMonotone: inserted " .. vi .. ":" .. M[vi][1] .. ", " .. M[vi][2])

		if walk_i_left == walk_i_right then
			break
		end
	end
	-- Leftover
	sorted_vertices[#sorted_vertices+1] = walk_i_left
	return sorted_vertices
--	for i = 1, #sorted_vertices do
--		vi = sorted_vertices[i]
--		print ("sorted vertex " .. vi .. ":" .. M[vi][1] .. ", " .. M[vi][2])
--	end
end

-- Input is a polygon M with at least 3 vertices given in order as they are connected
--       and the index in M of the vertex with the lowest x coordinate
-- Output is a table giving the indices of the hull the respective vertex belongs to
--       (1 = lower, 2 = upper, 3 = endpoints)
-- O(n) time, O(n) space
local function GetHullIndices(M, lowest_x)
	local whichHull = {}
	whichHull[lowest_x] = 1
	print ("hull vertex " .. lowest_x .. ":" .. whichHull[lowest_x])

	local walk_i_left =  SingleStepModulo(lowest_x-1, #M)
	local walk_i_right = SingleStepModulo(lowest_x+1, #M)
	local xl, xr
	-- Guaranteed different indices, as we have at least 4 vertices
	xl = M[walk_i_left][1]
	xr = M[walk_i_right][1]

	d = Determinant(M[walk_i_left], M[lowest_x], M[walk_i_right])

	-- We know that (in general position), the vertex at lowest_x is convex
	-- therefore we can figure out whether its neighbours are in lower or upper hull
	local rightHull = 1
	if d < 0 then -- right turn, forward walk is on upper hull, backward on lower
		rightHull = 2
	elseif d > 0 then -- left turn, forward walk is on lower hull, backward on upper
		rightHull = 1
	else
		error("Vertices are colinear!")
	end

	-- Walk forward through indices and copy active hull indicator
	local prevPoint = M[lowest_x]
	while true do
		local currPoint = M[walk_i_right]

		if currPoint[1] < prevPoint[1] then
			-- mark point with highest x (there may be many in general)
			whichHull[walk_i_right-1] = 3
			break -- end of hull reached
		end
		whichHull[walk_i_right] = rightHull
		print ("GetHullIndices: fwd " .. walk_i_right .. ":" .. rightHull)

		walk_i_right = SingleStepModulo(walk_i_right + 1, #M)
		prevPoint = currPoint
	end

	-- Walk backward through indices and copy active hull indicator
	if rightHull == 2 then
		rightHull = 1
	else
		rightHull = 2
	end

	prevPoint = M[lowest_x]
	while true do
		local currPoint = M[walk_i_left]

		if currPoint[1] < prevPoint[1] then
			break -- end of hull reached
		end
		whichHull[walk_i_left] = rightHull
		print ("GetHullIndices: bwd " .. walk_i_left .. ":" .. rightHull)

		walk_i_left = SingleStepModulo(walk_i_left - 1, #M)
		prevPoint = currPoint
	end

	for i = 1, #whichHull do
		local hi = whichHull[i]
		print ("hull vertex " .. i .. ":" .. (hi or "nil"))
	end
	return whichHull
end

-- Find index of first element by x coordinate
local function FindFirstPointByX(M)
	local lowest_x = 1
	for i = 2, #M do
		if M[i][1] < M[lowest_x][1] then
			lowest_x = i
		end
	end
	-- ... could be done in O(log #M).
	--print ("Lowest element is at index " .. lowest_x)
	return lowest_x
end

-- Output of this is not a DCEL as is being done in the book, but a list of
-- 3-vertex pairs for direct use in rendering
local function TriangulateMonotone(M)
	if #M < 4 then -- already a triangle or less
		return {M}
	end
	local triangles = {}

	-- Also we are not processing y-monotone, but x-monotone polygons
	-- First sort M by x coordinate. We keep two lists of indices, one of the
	-- upper and one for the lower hull.
	local upper_hull = {}
	local lower_hull = {}
	-- How do we know which side of lowest_x represents the lower hull?
	local lowest_x = FindFirstPointByX(M)

	sorted_vertices = SortMonotone(M, lowest_x)
	whichHull = GetHullIndices(M, lowest_x)

	stack = {sorted_vertices[1], sorted_vertices[2]}
	for i = 3, #sorted_vertices - 1 do
		-- Nomenclature:
		-- v: vertex being processed, vi: its index in M
		-- t: vertex that was on top of stack, ti: its index in M
		-- q: vertex that was below t on stack, qi: its index in M
		vi = sorted_vertices[i]
		v = M[vi]
		print ("")
		print ("Iteration " .. i .. ", vertex " .. vi .. " :")
		local str = "Stack:"
		for k,v in pairs(stack) do
			str = str .. " " .. v
		end
		print (str)

		ti = stack[#stack]
		-- are v and last of stack on the same path?
		if vi == ti + 1 or vi == ti - 1 then
			print ("Indices " .. vi .. " and " .. ti .. " are on the same path")
			-- A diagonal s(i-1), vi is valid when s(i-1), s(i), vi form a right turn
			-- i.e.   q, v is valid when q, t, v form a right turn

			-- "Pop one vertex from S"
			--stack[#stack] = nil

			qi = stack[#stack-1]
			while #stack > 0 do
				q = M[qi]
				t = M[ti]
				if (whichHull[vi] == 2 and IsRightTurn(q, t, v))
				or (whichHull[vi] == 1 and IsLeftTurn(q, t, v)) then
					-- Lower hull => left turns define legal diagonals
					-- Upper hull => right turns define legal diagonals
					triangle = {qi, ti, vi}
					print ("Triangle: " .. triangle[1] .. ", " .. triangle[2]
				    	   .. ", " .. triangle[3])
					table.insert(triangles, triangle)
				else
					break -- no point looking backward
				end

				ti = qi -- ti has been lost to a triangle, qi is our new top
				stack[#stack]= nil -- remove ti
				qi = stack[#stack] -- next top
			end
			-- "Push uj onto S"
			table.insert(stack, vi)

			--table.insert(stack, ti)
		else -- not on the same path
			for j = 2, #stack do
				triangle = {stack[j-1], stack[j], vi}
				print ("Triangle: " .. triangle[1] .. ", " .. triangle[2]
				       .. ", " .. triangle[3])
			end
			stack = {sorted_vertices[i-1], sorted_vertices[i]}
		end
	end

	-- Last triangle.
	triangle = {stack[1], stack[2], sorted_vertices[#sorted_vertices]}
	print ("Triangle: " .. triangle[1] .. ", " .. triangle[2]
	       .. ", " .. triangle[3])
end


--TriangulateMonotone(M)


local function GetMonotonePolygonHulls(M, leftmost_x)
	local walk_i_left  = SingleStepModulo(leftmost_x-1, #M)
	local walk_i_right = SingleStepModulo(leftmost_x+1, #M)
	local xl, xr
	-- Guaranteed different indices, as we have at least 4 vertices
	xl = M[walk_i_left][1]
	xr = M[walk_i_right][1]

	d = Determinant(M[walk_i_left], M[leftmost_x], M[walk_i_right])


	local fwd_hull = {leftmost_x}
	local bwd_hull = {leftmost_x}
	local highest_x = nil

	-- Walk forward through M
	local prevPoint = M[leftmost_x]
	while true do
		local currPoint = M[walk_i_right]

		if currPoint[1] < prevPoint[1] then
			-- mark point with highest x (there may be many in general,
			-- but this loop takes the one with the lowest y coordinate.
			rightmost_x = SingleStepModulo(walk_i_right-1, #M)
			break -- end of hull reached
		end
		table.insert(fwd_hull, walk_i_right)
		print ("GetMonotonePolygonHulls: fwd " .. walk_i_right)

		walk_i_right = SingleStepModulo(walk_i_right + 1, #M)
		prevPoint = currPoint
	end

	-- Walk backward through M

	prevPoint = M[leftmost_x]
	while walk_i_left ~= rightmost_x do
		local currPoint = M[walk_i_left]

		table.insert(bwd_hull, walk_i_left)
		print ("GetMonotonePolygonHulls: bwd " .. walk_i_left)

		walk_i_left = SingleStepModulo(walk_i_left - 1, #M)
	end
	table.insert(bwd_hull, rightmost_x)

	-- We know that (in general position), the vertex at leftmost_x is convex
	-- therefore we can figure out whether its neighbours are in lower or upper hull
	if d < 0 then -- right turn, forward walk is on upper hull, backward on lower
		return bwd_hull, fwd_hull
	elseif d > 0 then -- left turn, forward walk is on lower hull, backward on upper
		return fwd_hull, bwd_hull
	else
		error("Vertices are colinear!")
	end
end

M_lower,M_upper = GetMonotonePolygonHulls(M, FindFirstPointByX(M))
print("Lower:" .. ListToString(M_lower))
print("Upper:" .. ListToString(M_upper))


-- Returns the position of value in M, expressed in
-- terms of indices of list. [left_index, right_index].
-- If value is equal to some element, then the indices returned will be the same.
-- if value is outside, then left_index will be 0 and right_index
-- either positive (to the right) or negative.
local function Bisect(M, list, value)
	local lind = 1
	local rind = #list

	if value > M[list[#list]][1] then
		return 0, 1
	elseif value < M[list[1]][1] then
		return 0, -1
	end

	local mind

	while lind+1 < rind do
		mind = math.floor( (lind+rind) /2)

		if value > M[list[mind]][1] then
			lind = mind
		elseif value < M[list[mind]][1] then
			rind = mind
		else
			return mind, mind
		end
	end

	return lind, rind
end

-- Answers the question "is p in the polygon P"
-- input: M_lower/M_upper ... vertices of the lower and upper hulls
--          of an x-monotone polygon given in connected order from left to right.
--        p ... (x,y) point to locate
-- output boolean true/false
local function IsPointInMonotonePolygon(M, M_lower, M_upper, p)
	-- General procedure: find intersected segment su on the upper hull,
	--                                             sl        lower
	-- Check whether p lies between su, sl
	local ulind, urind = Bisect(M, M_upper, p[1])
	print("ulind: " .. ulind .. ", urind: " .. urind)

	if ulind == 0 then return false end -- point outside.

	local su_l = M[M_upper[ulind]]
	local su_r = M[M_upper[urind]]
	-- Is p below upper segment (su)?
	-- We already know that p has an x coordinate either above or below su,
	-- => left/right turn:
	if Determinant(su_l, su_r, p) > 0 then -- left turn, p lies above upper segment
		return false
	end


	local llind, lrind = Bisect(M, M_lower, p[1])
	print("llind: " .. llind .. ", lrind: " .. lrind)
	local sl_l = M[M_lower[llind]]
	local sl_r = M[M_lower[lrind]]
	if Determinant(sl_l, sl_r, p) < 0 then -- right turn, p lies below lower segment
		return false
	end

	return true
end

--IsPointInMonotonePolygon(M, M_lower, M_upper, {x = 2.3, y = 0.8})
local tf = IsPointInMonotonePolygon(M, M_lower, M_upper, {2.3, 0.8})
print ("Is in polygon? " .. ((tf and "true") or "false"))


---------------------------
-- non-monotone polygons --
---------------------------

local function SplitToMonotoneSortHelper(M, lhs, rhs)
	local l = M[lhs][1]
	local r = M[rhs][1]
	if l < r then
		return true
	elseif l == r and M[lhs][2] < M[rhs][2] then
		return true
	else
		return false
	end
end

-- Returns a list of polugons (vertices list) obtained by decomposing M
-- to a union of x - monotone polygons.
local function SplitToMonotonePolygons(M)
	local lowest_x = FindFirstPointByX(M)

	-- Sort vertices of M by x coordinate to prepare for line sweep
	-- We first require to copy M, so the vertex order (and thus our knowledge
	-- of edges) is preserved.
	local vertices = {} -- contains indices into M
	for i = 1, #M do
		vertices[i] = i
	end
	table.sort (vertices, function(l,r) return SplitToMonotoneSortHelper(M, l, r) end)
	print ("Sorted: " .. ListToString(vertices))

	-- Now that we have a list of sorted vertices, we can sweep.
	local T = {} -- State tree


end

NonMonotoneM = {
	{-2, 1},
	{2, 1},
	{3, 0},
	{2, -1},
	{-2, -1},
	{0, 0}
}

SplitToMonotonePolygons(NonMonotoneM)
